# How to write a runner for this project

Un paquet swift a généralement ce genre d'arborescence:

.
├── Package.swift
├── README.md
├── Sources
│   └── temp
│       └── main.swift
└── Tests
    └── tempTests
        └── tempTests.swift

4 directories, 4 files

Il vous faut prendre un runner qui utilise un conteneur "Swift".

